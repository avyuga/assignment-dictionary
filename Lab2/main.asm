%define buffer_size 256
%define STDERR 2

%include "words.inc"

global _start
extern string_length, print_string, read_word, exit
extern find_word

section .data

input: db '>> Enter a word >> ', 0
key: db '>> Key >> ', 0
error: db '>> Error occured, word not found.',10, 0


section .text

_start:
	mov rdi, input ; printing a starting line
	call print_string
	
	push rbp
	mov rbp, rsp
	sub rsp, 256
	mov rdi, rsp ; allocating memory on stack
	mov rsi, buffer_size ; amount of bytes to read (buffer size)
	call read_word
	
	mov rdi, rax  ; the word we are looking for in the dictionary
	mov rsi, last ; pointer to the first word in dictionary
	call find_word 

	test rax, rax ; if zero -> not found
	jz .not_found

	.found:
		push rax ; save the pointer
		mov rdi, key ; print '>> Key >>'
		call print_string
		pop rax ; restore pointer
		mov rdi, rax ; print the key
		call print_string
		mov rsp, rbp
		pop rbp
		mov rdi, 0 ; exit code to rdi
		call exit ; exit with code 0

	.not_found:
		mov rdi, error ; print error message
		push rdi
		call string_length ; the result is in rax
		pop rdi
		mov rdx, rax ; 	move result number of characters to rdx
		mov rax, 1 ; WRITE syscall number
		mov rsi, rdi ; put the output string aka argument in rsi
		mov rdi, STDERR ; stderr descriptor
		syscall

		mov rsp, rbp
		pop rbp
		mov rdi, 0
		call exit
		ret 
	
	
