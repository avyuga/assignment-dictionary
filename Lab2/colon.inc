%define last 0
%macro colon 2
	dq last
	%2: db %1, 0
%define last %2
%endmacro
